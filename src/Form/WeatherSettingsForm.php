<?php

namespace Drupal\current_weather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WeatherSettingsForm.
 *
 * @package Drupal\current_weather\Form
 */
class WeatherSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'current_weather_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['current_weather.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('current_weather.settings');

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City name'),
      '#required'  => TRUE,
      '#default_value' => $config->get('city'),
    ];

    $form['country_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Country code'),
      '#required'  => TRUE,
      '#default_value' => $config->get('country_code'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required'  => TRUE,
      '#default_value' => $config->get('api_key'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('current_weather.settings')
      ->set('city', $form_state->getValue('city'))
      ->set('country_code', $form_state->getValue('country_code'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
