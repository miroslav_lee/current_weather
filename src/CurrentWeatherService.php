<?php

namespace Drupal\current_weather;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;

/**
 * Class CurrentWeatherService.
 *
 * @package Drupal\current_weather
 */
class CurrentWeatherService {

  /**
   * The url request to openweathermap api.
   */
  const API_URL = 'https://api.openweathermap.org/data/2.5';

  /**
   * The Current weather settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * CurrentWeatherService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    $this->settings = $config_factory->get('current_weather.settings');
    $this->httpClient = $http_client;
  }

  /**
   * Call current weather data for one location.
   *
   * @param string $city
   *   The city name.
   * @param string $country_code
   *   The Country code value.
   *
   * @return array
   *   The weather information.
   */
  public function getCurrentWeatherByCity(string $city = '', string $country_code = '') {
    if (!$city) {
      $city = $this->settings->get('city');
      $country_code = $this->settings->get('country_code');
    }

    $url = Url::fromUri(self::API_URL . '/weather', [
      'query' => [
        'q' => "{$city},{$country_code}",
      ],
    ]);
    $json_response = $this->request($url)->getBody()->getContents();

    return Json::decode($json_response);
  }

  /**
   * Request to the OpenWeatherMap API server.
   *
   * @param \Drupal\Core\Url $url
   *   The url object.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Representation of an outgoing, server-side response.
   */
  protected function request(Url $url) {
    $query = $url->getOption('query');
    $query['appid'] = $this->settings->get('api_key');
    $url->setOption('query', $query);

    return $this->httpClient->get($url->toString());
  }

}
