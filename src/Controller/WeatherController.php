<?php

namespace Drupal\current_weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\current_weather\CurrentWeatherService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Current Weather module routes.
 */
class WeatherController extends ControllerBase {

  /**
   * The Current weather API service.
   *
   * @var \Drupal\current_weather\CurrentWeatherService
   */
  protected $weatherService;

  /**
   * WeatherController constructor.
   *
   * @param \Drupal\current_weather\CurrentWeatherService $weather_service
   *   The Current weather API service.
   */
  public function __construct(CurrentWeatherService $weather_service) {
    $this->weatherService = $weather_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_weather.api_service')
    );
  }

  /**
   * Builds the weather information.
   *
   * @return array
   *   Render array containing a theme.
   */
  public function build() {
    try {
      $info = $this->weatherService->getCurrentWeatherByCity();

      $build = [
        '#theme' => 'weather',
        '#city' => $info['name'],
        '#temp' => round($info['main']['temp'] - 273.15) . '°C',
        '#humidity' => $info['main']['humidity'] . '%',
        '#description' => $info['weather'][0]['description'],
      ];
    }
    catch (\Exception $exception) {
      $build = [
        '#markup' => $this->t('Something wrong. Check the module settings.'),
      ];
    }

    return $build;
  }

}
